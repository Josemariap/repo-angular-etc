//https://www.youtube.com/watch?v=vL85o-hYBUI&list=PLGIvoD9Ur9I3_3NcQRIV3lBs9srqwYAki&index=6
//Para mensajes FLASH con node, usar librerias connect-flash /npm install connect-flash /en app.js hacer el require del modulo y app.use(flash())
//Para mensajes inicio de sesion libreria express-session / npm install express-session   -->libreria para manejo de sesiones
//Para autentidficacion libreria passport y passport-local /npm install passport / npm install passport-local   -->estrategia local no usa nada externo para el login ni api externa ni db externa
var mysql = require('mysql')

var bcrypt = require('bcryptjs')  //Instalamos esta dependencia en el proyecto: npm install bcryptjs, modulo para encriptar password

module.exports = {
  
  
  getSignUp : function(req, res, next){
     return res.render('users/signup')  //Retorna la vista de registro de usuarios
  }, 

 
  postSignUp : function(req, res, next){
   
     var salt = bcrypt.genSaltSync(10)//Metodo con el level 10 de dificultad para encripatado
     var password = bcrypt.hashSync(req.body.password, salt)//Metodo hash para encriptar 
     
     var user = {
       email : req.body.email,
       nombre : req.body.nombre,
       password : password
     }

     var config = require('.././database/config')
     var db = mysql.createConnection(config)
     db.connect()
     db.query('INSERT INTO users SET ?', user, function(err, rows, fields){
         if(err) throw err

         db.end()

     })
     //Este mensaje flash viajara solo cuando se redireccione del registro correcto
     req.flash('info', 'Se ha registrado con éxito, ya puede iniciar sesión')//Crea mensaje flash de registro/recuperar en la vista
     return res.redirect('/auth/signin')  //Hace la peticion y la atiende la ruta y dirige hacia el login
  },


  getSignIn : function(req, res, next){
       return res.render('users/signin', {message : req.flash('info'), authmessage : req.flash('authmessage')})//Capturamos en la vista por su nombre message
  },

  
  //Funcion de cierre de sesion de passport y redirecciona al inicio de sesion 
  logout : function(req, res, next){
     req.logout();
     res.redirect('/auth/signin');//peticion para redirigir al inico de sesion
  },


  getUserPanel : function(req, res, next){
    res.render('users/panel', {
      isAuthenticated : req.isAuthenticated(),
      user : req.user
    });
  }




}






/*
  database: tutorialnodejs/tabla: users
  
  tabla users

  mail: homero@gmail.com
  password: 1234 
 
  mail : marge@gmail.com
  password : 123

  mail : bart@gamil.com
  password : 12345

  mail : otto@gmail.com
  password : otto

 */