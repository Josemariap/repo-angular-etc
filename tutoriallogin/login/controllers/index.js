/*Creamos rutina de codigo que nos importe automaticamente cada unos de los controladores, para no tener que hacerlos en las rutas
Funciona com o un autocargador */

var fs = require('fs');  //fs es el sistema de archivos de node

var path = require('path');

var files = fs.readdirSync(__dirname); //lee todos los archivos que esten el directorio indicado por parametro (carpeta controllers)
//Todos los archivos que estan en la carpeta controllers ahora estan en files
//Recorremos los archivos de var files
files.forEach(function(file){

   var fileName = path.basename(file, '.js')  //Gardo la ruta del archivo y le agrego la extencion como segundo parametro
 
   if(fileName !== 'index'){
     exports[fileName] = require('./'+ fileName);//Todas las rutas completas de los archivos de controllers quedaran guardas en un array llamado exports
   }
//Esto sera nuestro autocargador de archivos de controllers
});