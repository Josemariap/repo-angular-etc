module.exports = {
  
  //La auntenticacion correcta redirecciona al home, y le envia el user logueado, en el home capturamos el user
  index : function(req, res, next ){
    res.render('home', {
    	isAuthenticated : req.isAuthenticated(),//le pasomos al home la autenticacion 
    	user : req.user  //Le pasa al home el user correcto
    })
  }


}