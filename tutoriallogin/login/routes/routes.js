var express = require('express');
var router = express.Router();
var passport = require('passport');
var controllers = require('.././controllers');
var AuthMiddleware = require('.././middleware/auth');  //Para no entrar por url a las paginas protegidas

/* GET Inicio page. */
router.get('/', controllers.HomeController.index)

/*GET form registro users*/
router.get('/auth/signup', controllers.UserController.getSignUp)

/*POST guarda usuario*/
router.post('/auth/signup', controllers.UserController.postSignUp)

/*GET inicio de sesion de users*/
router.get('/auth/signin', controllers.UserController.getSignIn)

/*autentificacion-Iniciar sesion / actua el fichero passport.js donde comprueba el login contra la db*/
router.post('/auth/signin', passport.authenticate('local', {
	successRedirect : '/users/panel', //Redireccion(peticion) si la autentidicacion es correcta y viaja con el user logeado 
	failureRedirect : '/auth/signin',  //Redireccion al login si falla la autentificacion
	failureFlash : true    //Cuando falla la autentificaciion activamos el envio de message flash
}) );

router.get('/auth/logout', controllers.UserController.logout)

//Peticion de perfil
//Tiene un middleware para no entrar al panel user por url comprbando si estamos o no logeados para dar paso
router.get('/users/panel', AuthMiddleware.isLogged, controllers.UserController.getUserPanel)  //Esta  peticion estara protegida por middleware


module.exports = router;

