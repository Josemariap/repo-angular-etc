
/*La funcion de autentificacion en node son 3 pasos:
  serializar el objeto una vez autentificado el usuario, deserializar el objeto y tercero autenticar
  passport tiene metodos para esto....
*/

var LocalStrategy = require('passport-local').Strategy;
var mysql = require('mysql');
var bcrypt = require('bcryptjs')

module.exports = function(passport){

   passport.serializeUser(function(user, done){
   	   done(null, user);
   });

  
   passport.deserializeUser(function(obj, done){
      done(null, obj);
  });


//Autentificacion
   passport.use(new LocalStrategy({
      passReqToCallback : true
   }, function(req, email, password, done){
       
        var config = require('.././database/config')
        var db = mysql.createConnection(config)
        db.connect()
        db.query('SELECT * FROM users WHERE email=?', email, function(err, rows, fields){//Obtenmos el registro por email
        if(err)throw err

        db.end()
        
        if(rows.length > 0) { // si hay un registro se guarda en user
          var user = rows[0];
        //Comparamos los password del registro con el del formulario de inicio de sesion
          if(bcrypt.compareSync(password, user.password)){ //Compara y devuelve true o false
             return done(null, {
                id : user.id,
                nombre : user.nombre,
                email : user.email
             } );
          }
        }
        return done(null, false, req.flash('authmessage', ' Password o email incorrectos'));

        })
   }
   ))


};