//Este middleware comprueba si el usuario esta logeado o no, para no poder entrar al panel de usuario por url
//Un middleware en node es lo que esta entre medio del request y el response!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//Esta middleware se colocara a todas las url que queremos proteger
/*Cuando se haga cualquier peticion siempre pasara por este middleware, pero debera estar el middleware en la ruta como parametro*/
/*El middleware se coloca en las rutas para interceptar las peticiones, de la peticion que queramos proteger u otras funciones del middleware*/







module.exports = {

   isLogged : function(req, res, next){
      if(req.isAuthenticated()){
         next();  //Si esta autenticado continua con la peticion 
      }else{
         res.redirect('/auth/signin');  //Si el user no esta autenticado redirecciona al login, evitando entrar por url a los home user
      }
   }


}