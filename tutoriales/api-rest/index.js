'use strict'
//Librereia nodemon :  se usa para no tener que reiniciar el servidor cada vez que hacemos un cambio. Se actualiza y se reinicia
//el servidor: en el package.json  dentro del script colocar   "start": "nodemon index.js", y ejecutar npm start por consola ubicado dentro del proyecto
//luego de esto al hace ctrl+s luego de modificar el codigo, se reiniciara autom. el servidor

//Libreria body-parser : para que node pueda capturar el cuerpo de las peticiones json

//Libreria Express nos genera la extructura del proyecto, nos permite manejar mvc, el package.json podemos usar las dependencias etc.. etc..

//Libraria ORM "mongoose": "^4.7.1" driver mongoose en nuestro proyecto node para conectarnos a mongo db, se instala con comando: npm i -S mongoose

const express = require('express')
const bodyParser = require('body-parser')  //Modulo para que Node pueda capturar el cuerpo de las peticiones en json
const mongoose = require('mongoose') //Para realizar la conexion a mongo y demas operaciones con mongo

const Product = require('./models/product')//Asignamos el esquema producto usando mongoose

const app = express()
const port = process.env.PORT || 3001 //Qe use de puerto una variable de entorno o 3000

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json()) //Para permitir el cuerpo del msj en formato json

//
/*VERBOS HTTP*/
//El servidor atendera la peticion hola que es de tipo get
//Si no recibira parametros en la peticion quitar  :name  
/* Peticion get prueba

app.get("/hola/:name",  function(req, res) {
	res.send({message : `Hola desde node js, Metodo get, Saludos ${req.params.name}`})//Usamos response para responder, en formato json
} )

*/  //arrows function : => = callback = funcion anonima

//Instalar postman plugin de  chrome para simular cliente Rest etc../ ir a plugin instalados y crear acceso directo de postman
//postman nos permite realizar las peticiones http y enviar datos en el body de la peticion

//Usamos los metodos crud de mongoose
app.get("/api/product", (req, res) => {
    Product.find({}, (err, products) => {
        if (err) return res.status(500).send({ message: `Error al realizar la peticion: ${err}` })
        if (!products) return res.status(404).send({ message: `No exixten productos` })

        //productos encontrados
        res.send(200, { products })
    })
})


app.get("/api/product/:productId", (req, res) => {
    let productId = req.params.productId

    Product.findById(productId, (err, product) => {
        if (err) return res.status(500).send({ message: `Error al realizar la peticion: ${err}` })
        if (!product) return res.status(404).send({ message: `El producto no existe` })

        //Producto encontrado
        res.status(200).send({ product })//Mustro producto json (manera simplificada)

    })
})


app.post("/api/product", (req, res) => {
    //console.log(req.body)   // los datos del nuevo producto vienen en el body de la peticion, probar POSTMAN de chrome
    //res.status(200).send({message : 'El producto se ha recibido con exito'})
    console.log("POST/api/product")  //mostramos la peticion
    console.log(req.body)  //Chequeamos que llega en el body de la peticion (datos del producto nuevo)
    // let declara una variable de alcance local, la cual, opcionalmente, puede ser inicializada con algún valor
    let product = new Product() //Cramos nuestro producto y le asignamos los valores que vienen en el body de la peticion
    product.name = req.body.name  //ya viene en el body parseado json por la libreria body-parser
    product.picture = req.body.picture
    product.price = req.body.price
    product.category = req.body.category
    product.description = req.body.description

    product.save((err, productStored) => {
        if (err) res.status(500).send({ message: `Error al salvar en la base de datos: ${err}` })// Solo si hay un error en save

        //Si se guarda con exito entonces...
        res.status(200).send({ product: productStored })
        //El servidor si se guardo  correctamente nos devuelve codigo 200 y lo que se guardo mas el id que le asigno mongo

    })
})


app.put("/api/product/:productId", (req, res) => {
    let productId = req.params.productId//De la cabecera capturo el id
    let update = req.body//Del cuerpo de la peticion capturo todos los datos de actualizacion

    //Metodo de mongoose actualizar
    Product.findByIdAndUpdate(productId, update, (err, productUpdated) => {
        if (err) return res.status(500).send({ message: `Error al actualizar el producto: ${err}` })

        res.status(200).send({ product: productUpdated })
    })
})


//Busca el producto por id y luego lo elimina
app.delete("/api/product/:productId", (req, res) => {
    let productId = req.params.productId

    Product.findById(productId, (err, product) => {
        if (err) return res.status(500).send({ message: `Error al borrar el producto: ${err}` })

        product.remove(err => {
            if (err) return res.status(500).send({ message: `Error al borrar el producto: ${err}` })

            res.status(200).send({ message: 'El producto ha sido eliminado' })
        })
    })
})







/*Servidor y conexion a mongo. La db se llamara shop*/
//Si se produce  un error en la conexion lanzara el error y saldra del metodo, si la conexion se realiza inicia el servidor
mongoose.connect('mongodb://localhost:27017/shop', (err, res) => {
    if (err) {
        return console.log(`Error al conectarse a la base de datos: ${err}`)
    }
    //Si no hay error al conectarse a mongo se inicia el servidor
    console.log("Conexion a la base de datos mongo establecida...")
    app.listen(port, function () {

        console.log(`API REST corriendo en http://localhost:${port}`) //usar comilla invertida del acento para que tome la variable
    })

})

