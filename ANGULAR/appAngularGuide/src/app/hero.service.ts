import { Injectable } from '@angular/core';
import {Hero} from "./hero";
//import {HEROES} from "./mock-heroes"; //HERO es una const que tiene un array de heroes
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export  class HeroService{
  private heroesUrl = 'api/heroes'; // URL para la web api
  constructor(private http: Http) { }

/*getHeroes cuando usaba el mock-heroes.ts y retornaba promesa resuelta con array de heroes
  getHeroes(): Promise<Hero[]>{    //El metodo getHeroes retorna una promesa resuelta con  un array del tipo Hero
    return Promise.resolve(HEROES); // este return sera el argumento del then
  }*/


  //El método http.get de Angular devuelve un RxJS Observable. Los Observables son una forma poderosa de administrar los flujos de datos asíncronos
  // con toPromise lo convertimos en promesa, sin toPromise los metodos http devuelven un obserbavable
  getHeroes(): Promise<Hero[]> {
    return this.http.get(this.heroesUrl) //le pasamos la var con la url de la web api que reconoce a la clase InMemoryDataService que ejecuta el metodo createDb  y retorna el array heroes
      .toPromise()  //todos los métodos de Http devuelve un Observable de un objeto HTTP Response, HeroService convierte ese Observable en una Promise y devuelve la promesa a la función de llamada.
      .then(response => response.json().data as Hero[])//data contiene el array de heroes que es el response del get y lo hacemos un array de tipo Hero
      .catch(this.handleError);//hendleError es un promesa no cumplida
  }

  private handleError(error: any): Promise<any> {
    console.error('Ocurrio un error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  //usando la web api
  getHero(id: number): Promise<Hero> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Hero) //data es un solo objeto
      .catch(this.handleError);
  }

  /*
  //retorna un promesa con un heroe buscado por id sin usar la web api
  getHero(id: number): Promise<Hero> {
    return this.getHeroes()
      .then(heroes => heroes.find(hero => hero.id === id));
  }*/


  private headers = new Headers({'Content-Type': 'application/json'});

  update(hero: Hero): Promise<Hero> {
    const url = `${this.heroesUrl}/${hero.id}`; //el id es para ubicar cual actualizar en el servidor
    return this.http
      .put(url, JSON.stringify(hero), {headers: this.headers})//peticion put para actualizar en el servidor y le pasamos el hero que actualizara
      .toPromise()
      .then(() => hero)
      .catch(this.handleError);
  }


  create(name: string): Promise<Hero> {
    return this.http
      .post(this.heroesUrl, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Hero) //en el res viene el nuevo heroe agregado en el servidor web
      .catch(this.handleError);
  }

  delete(id: number) {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
}


