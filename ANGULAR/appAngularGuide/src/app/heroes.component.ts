import {Component, OnInit} from '@angular/core';
import {Hero} from "./hero";
import { HeroService } from './hero.service';
import {Router} from "@angular/router";

@Component({
  selector: 'my-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css'],
  providers: [HeroService]  //Indicamos para hacer la inyeccion de dependencia de la clase y si se inyecta en muchos compnentes, podemos indicarlo en el module
})


export class HeroesComponent {
  selectedHero: Hero;
  heroes: Hero[];

  constructor(private heroService: HeroService, private router: Router) {//Inyeccion de dependencia HeroService y Router
    this.heroService.getHeroes().then(heroes => this.heroes = heroes); //en el constructor se carga el array con el metodo getHeroes, para que se visualicen en el template
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

  add(name: string): void {
    name = name.trim();   //quita los espacios en blanco por delante y detras
    if (!name) { return; } //si name esta vacio entra al if
    this.heroService.create(name)
      .then(hero => {
        this.heroes.push(hero);  //agrega el heroe al array para que actualice la vista
        this.selectedHero = null;
      });
  }

  delete(hero: Hero): void {
    this.heroService
      .delete(hero.id)
      .then(() => {
        this.heroes = this.heroes.filter(h => h !== hero);   //borra del array para que actualice la vista
        if (this.selectedHero === hero) { this.selectedHero = null; }
      });
  }

}
