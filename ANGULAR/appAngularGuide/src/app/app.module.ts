import { BrowserModule }   from '@angular/platform-browser';
import { NgModule }        from '@angular/core';
import { FormsModule }     from '@angular/forms';
import { HttpModule }      from '@angular/http';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service'; //clase creada

import {HeroesComponent}    from './heroes.component';
import {HeroDetailComponent}from './hero-detail.component';
import {AppComponent}       from "./app.component";
import {DashboardComponent} from "./dashboard.component";

import { HeroService }      from './hero.service';

import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent, HeroDetailComponent, HeroesComponent, DashboardComponent ],
  imports: [               /*AppRoutingModule --> json de enrrutamientos de component*/
    BrowserModule, FormsModule, AppRoutingModule, HttpModule, InMemoryWebApiModule.forRoot(InMemoryDataService) ],
  providers: [HeroService], // indicar que esta clase sera inyectada, la podremos inyectar en muchos componentes
  bootstrap: [AppComponent]
})
export class AppModule { }
