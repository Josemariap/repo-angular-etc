import {Component, Input, OnInit}   from '@angular/core';
import {Hero}                       from "./hero";
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import { HeroService }              from './hero.service';
import 'rxjs/add/operator/switchMap';

//Esta componente se invoca por ruta (id param) y por selector ( pasa el objeto el padre )
@Component({
  selector: 'hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: [ './hero-detail.component.css' ]
})


export class HeroDetailComponent implements OnInit{
  @Input() hero: Hero   //El input es para recibir el objeto hero del componente padre que invoca a este componente y por el metodo ngOnInit
  //A este componente tambien lo invoco por ruta desde el dashboard y le paso un id de param para el ngOnInit
  constructor(
    private heroService: HeroService,//inyeccion de dependencia del service HeroService, etc.
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.paramMap //metodo para caputar parametros
      .switchMap((params: ParamMap) => this.heroService.getHero(+params.get('id')))
      .subscribe(hero => this.hero = hero);
  }

  goBack(): void {
    this.location.back();
  }


  save() : void {
    this.heroService.update(this.hero)
      .then(() => this.goBack());
  }

}
