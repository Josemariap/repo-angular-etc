import { AppAngularGuidePage } from './app.po';

describe('app-angular-guide App', () => {
  let page: AppAngularGuidePage;

  beforeEach(() => {
    page = new AppAngularGuidePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
