import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-headers></app-headers>
    <div class="row">
      <div class="col-xs-12 col-md-12 col-lg-4">
        <app-left-navigation></app-left-navigation>
      </div>
      <div class="col-xs-12 col-md-12 col-lg-8">
        <app-right-navigation></app-right-navigation>
      </div>
    </div>
    <app-footer></app-footer>`,
})
export class AppComponent {

}
