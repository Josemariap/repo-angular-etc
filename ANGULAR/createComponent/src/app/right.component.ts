import { Component } from '@angular/core';


@Component({
  selector: 'app-right-navigation',
  template: `<p class="right-style">  </p>
  <ul>
    <li *ngFor="let item of items">{{item}}</li>
  </ul>`,
  styleUrls: ['./styles.component.css']
})

export class RightComponent {
    items:Array<string> = ['Noticias', 'Eventos', 'FAQS']
}

