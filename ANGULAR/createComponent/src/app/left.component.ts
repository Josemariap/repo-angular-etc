import { Component } from '@angular/core';


@Component({
  selector: 'app-left-navigation',
  template: `<h2><p class="left-style"> {{title}}  </p></h2>`,
  styleUrls: ['./styles.component.css']
})

export class LeftComponent {
  title:string =  "News";
}
