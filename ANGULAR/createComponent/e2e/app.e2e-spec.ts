import { CreateComponentPage } from './app.po';

describe('create-component App', () => {
  let page: CreateComponentPage;

  beforeEach(() => {
    page = new CreateComponentPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
