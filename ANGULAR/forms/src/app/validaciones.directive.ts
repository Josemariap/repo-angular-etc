import { Directive } from '@angular/core';
import  { AbstractControl, NG_VALIDATORS} from "@angular/forms"


function verificarEspacios (c: AbstractControl) {
  if (c.value == null) {
    return null
  }
  if (c.value.indexOf(" ") >= 0) {
    return {sinEspacion: true}
  }
}
// Ver más de formularios / form reactive --> https://www.youtube.com/watch?v=hNlb1Fzzp8s&list=PLNeY9BVjevaqDItPV1PmRoe93eNalL7JH&index=4
