import { Component } from '@angular/core';
//Este component renderizara una vista
@Component({    //Esto es un decorador y va endosado a una clase component que es la que se exporta abajo
  selector: 'app-root',
  templateUrl: './app.component.html', //esta decorador indica el template sobre el que dara apoyod
  styleUrls: ['./app.component.css']
})
export class AppComponent {   //Esta class actua como un contralador sobre el template o vista
  name : string ; //definimos el tipo
  mail = 'aquiles@gmail.com';
  web = 'www.aquiles.com';
  hobbies = ['correr', 'dormir', 'saltar'];
  verHobbies : boolean;

   constructor(){  //Seteo mi variable name
     console.log('Dentro del contructor');
     this.name='Agamenon';
     this.verHobbies= false; //al estar en false oculta los hobbies en la vista, con el metodo cambia cuando se llama desde el boton
   }
   
  showHobbies(){ //metodo usado desde la vista en un div en la directiva ngIf
    this.verHobbies= !this.verHobbies;  //asigna lo contrario de lo que contiene o true o false
  }
}
