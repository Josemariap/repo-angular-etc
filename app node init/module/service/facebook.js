var requester = require("request");
var querystring = require("querystring");
var config = require('../../config/config');

function postMessage(groupId, body) {

    return new Promise((resolve, reject) => {

        var data = {
            message: 'otro saludo',
        };

        var formData = querystring.stringify(data);
        var contentLength = data.length;

        requester({
            headers: {
                'Content-Length': contentLength,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            uri: config.facebook.url + groupId + '/feed/?access_token=' + config.facebook.token,
            body: formData,
            method: 'POST'
        }, function (err, res, body) { resolve(body) });



    })



}

module.exports = {
    postMessage
}