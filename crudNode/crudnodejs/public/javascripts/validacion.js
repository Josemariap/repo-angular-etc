$(function(){
	$('.form-nuevoproducto form').form({
		nombre : {
			identifier : 'nombre',
			rules : [
			 {
				type: 'empty',
                prompt: 'Por favor ingrese nombre del producto'
			 }
		   ]
		},

		precio : {
			identifier : 'precio',
			rules : [
			 {
                type: 'empty',
                prompt: 'Por favor ingrese precio del producto'
             },
             {
             	type: 'number',
             	prompt: 'El precio debe ser numerico'
             }

		   ]
		},

		stock : {
			identifier : 'stock',
			rules : [
			 {
                type: 'empty',
                prompt: 'Por favor ingrese stock del producto'
             },
             {
             	type: 'integer',
             	prompt: 'El stock debe ser un número entero'
             }

		   ]
		}
	})
})