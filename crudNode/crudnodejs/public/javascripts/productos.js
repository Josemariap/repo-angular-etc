$(function(){
	//Funcion Ajax eliminar producto
	$('#tbl-productos #btn-eliminar').click(function(e){
		e.preventDefault()
		var elemento = $(this)  //Guardo el elemento al que se clikeo
		var id= elemento.parent().parent().find('#id_producto').text()//Capturo el id  

		var confirmar = confirm('Desea eliminar el producto?') //Ventana alert de confirmacion de eliminacion
		if(confirmar){
		//paticion Ajax- Realiza peticion y envia el id
	 	  $.ajax({
			url : 'http://localhost:3000/eliminarproducto',
			method :'post',
			data : {id: id},
			success: function(res){ //res contiene la respuesta de ajax
                if(res.res){
                	elemento.parent().parent().remove()
                }
              }
		  }) 
	    }//end if

	})
})