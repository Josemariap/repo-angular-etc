//Home controller
// https://www.youtube.com/watch?v=MxpDHzmWBEM&index=3&list=PLGIvoD9Ur9I01Oa-2IOdXsuSZLIHGNxvH
//En el  modulo exports nos guardara nuestras funciones creadas y nos permite hacer el require de este modulo y usar los metodos
module.exports = {
  //Funciones del controlador
  //Creamos funcion llamada index
  //Este metodo sera llamado de nuestro archivo de rutas routes.js
  index : function(req, res, next){
  	res.render('index', {title: 'Bienvenidos al crud con Node js'});
  }

}