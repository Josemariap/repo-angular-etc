//Producto controller
// https://www.youtube.com/watch?v=g-_l_v_uX6E&index=4&list=PLGIvoD9Ur9I01Oa-2IOdXsuSZLIHGNxvH
//En el  modulo exports nos guardara nuestras funciones creadas y nos permite hacer el require de este modulo y usar los metodos

var mysql =require('mysql')
var dateFormat = require('dateformat')

module.exports = {
  //Funciones del controlador
  //Creamos funcion llamada index
  //Este metodo sera llamado de nuestro archivo de rutas routes.js
  getProductos : function(req, res, next){

    var config = require('.././database/config')
    var productos = null;

    var db = mysql.createConnection(config) 
    db.connect();
 
    db.query('SELECT * FROM productos', function(err, rows, fields){
        if(err) throw err;

        productos = rows;
        db.end();
        res.render('productos/productos', {productos: productos} )//Rendariza la vista y le envia los productos de la db
    })//end query
  	
  },//end function getProductos

  

  getNuevoProducto : function(req, res, next){
       res.render('productos/nuevo')
  },//end function getNuevoProducto


  postNuevoProducto : function(req, res, next){
  	//console.log(req.body)

  	var fechaactual = new Date()
    var fecha = dateFormat(fechaactual, 'yy-mm-dd h:MM:ss')//utiliza el modulo

    var producto = {   //Los atributos tienen el mismo nombre que las columnas de la db y el valor del atributo como los name del form
    	nombre: req.body.nombre,   
    	precio: req.body.precio,
    	stock: req.body.stock,
    	fecha_creacion: fecha
    }
   //console.log(producto)
   var config = require('.././database/config')
   var db = mysql.createConnection(config) 
   db.connect();
   db.query('INSERT INTO productos SET ?', producto, function(err, rows, fields){
   	  if(err) throw err;  

   	  db.end()
   })
   res.render('productos/nuevo', {info: 'Producto creado correctamente'})

  },//end function postNuevoProducto


  eliminarProducto : function(req, res, next){
    var id = req.body.id

    var config = require('.././database/config')
    var db = mysql.createConnection(config) 
    db.connect();
    db.query('DELETE FROM productos WHERE id_producto = ?', id, function(err, rows, fields){
   	 
    var respuesta = {res: false}
   	if(err) throw err  //Si hay arror sale del metodo y res queda en false
   	     
   	db.end()
    respuesta.res = true   //Si la consulta se realizo con éxito res queda en true
   	res.json(respuesta)   //Si res es true tambien devuelve un objeto json con respuesta
 
   })
  },//end eliminarProducto


  getModificarProducto : function(req, res, next){
    var id = req.params.id//captura el id enviado en la peticion
    
    var config = require('.././database/config')
    var db = mysql.createConnection(config) 
    db.connect();

    var pruducto = null
    db.query('SELECT * FROM productos WHERE id_producto= ?', id, function(err, rows, fields){
   	 
    	if(err) throw err  //Si hay arror sale del metodo y res queda en false
   	
   	producto = rows
   	db.end()
   	res.render('productos/modificar', {producto : producto})
   
   })
  },//end modificarProducto
 

postModificarProducto : function(req, res, next){
   var producto = {   //Los atributos tienen el mismo nombre que las columnas de la db y el valor del atributo como los name del form
    	nombre: req.body.nombre,   
    	precio: req.body.precio,
    	stock: req.body.stock
    }

    var config = require('.././database/config')
    var db = mysql.createConnection(config) 
    db.connect();

    db.query('UPDATE productos SET ? WHERE ?', [producto, {id_producto : req.body.id_producto}], function(err,  rows, fields){
    	if(err) throw err
    	db.end()
    		
    })

    res.redirect('/productos')
  }//end postModificarProducto
 

}//end exports