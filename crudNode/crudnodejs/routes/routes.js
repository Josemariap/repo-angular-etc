/*Atencion, agregar archivos semantic en la carpeta public crear un carpeta semantic, agregar jquery en public javascripts
Hay validacion de campos en la carpeta javascripts*/
/*Esta archivo de rutas nos retornara las vistas*/

var express = require('express'); //Rutas Express
var router = express.Router();

var controllers = require('.././controllers') //Cuando entre a la carpeta controllers primero
//buscara un archivo llamado index, cuando ejecute su codigo autocargara los controladores y como se estan exportando 
//nos los guardara en nuestra variable controllers para usarlos en este archivo

/* GET home page, ruta que se cargara por defecto, retornara el index*/
router.get('/', controllers.homecontroller.index ); //Esta ruta finalmente llama al metodo index de homecontroller y este metodo 
//nos renderiza la vista index de la carpeta views

/*Ruta para productos*/
router.get('/productos', controllers.productoscontroller.getProductos);

/*Ruta para nuevo productos*/
router.get('/nuevo', controllers.productoscontroller.getNuevoProducto);

/*Ruta form action nuevo producto*/
router.post('/crearproducto', controllers.productoscontroller.postNuevoProducto);

/*Ruta para eliminar producto*/
router.post('/eliminarproducto', controllers.productoscontroller.eliminarProducto);


/*Ruta para ir al formulario de actualizar producto*/
router.get('/modificar/:id', controllers.productoscontroller.getModificarProducto);

/*Ruta para modificar y confirmar actulizar producto*/
router.post('/editar', controllers.productoscontroller.postModificarProducto);


module.exports = router;
